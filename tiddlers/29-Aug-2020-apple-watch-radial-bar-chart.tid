created: 20200829095626503
modified: 20210528013724384
tags: R-programming SkateAnalytics Journal DataVisualization
title: 29-Aug-2020-apple-watch-radial-bar-chart
tmap.id: 0613d676-ad1c-4ec3-b0da-452741a7857a
type: text/vnd.tiddlywiki

It would be very cool to visualize the stats of a skate session (or general group of sessions, like a month) in a similar way as they do with exercise on the Apple Watch.

I was inspired to think of this by seeing [[this tweet about how the author had been motivated to exercise more by the gamifying aspects of the Apple Watch|https://twitter.com/patio11/status/1299637471085355008?s=20]], which includes a [[screenshot|https://pbs.twimg.com/media/Egk9GksUcAEMxa1?format=jpg&name=large]] of a calendar for this month with each day including a visual representation of the characteristics associated with each day's exercise --- the visual makes it readily apparent which days he did or did not exercise and, presumably, when he did more or less. The visualization is quite beautiful, too: against a dark background, there's miniature circle plots (radial projections encircling a centerpoint) that are vividly colored on days with exercise, and they are apparently made the most vivid on days with better/more exercise and less vivid on days with less/none.

I suspect that these visualizations are to a great degree their own reward for using the system, putting aside whatever additional gamifying aspects there are in using an Apple Watch for exercise.

With the Apple Watch inspiration, I don't know what each band of the radial loops/rings is supposed to represent --- that is, I don't know what characteristics about that particular day's exercise it's plotting on each ring. However, I know what I could do with that for SkateAnalytics (below)!

Let's stick for now with the idea of visualizing data for an individual day, and let's assume that only one skate session happened in that one day (though if multiple sessions occurred, we could aggregate the sessions to provide a look at the day in total or break it down into sub-visualizations of each individual session, or both!).

Each ring could represent a particular trick that was attempted that day. Let's assume for now that we don't super need/want to know which loop accords to which trick (this frees us from needing to worry about color-coding or labeling the loops --- after all, the Apple Watch visualization didn't bother with labels, so why should we?).

How to order the rings? I think that the loop that is furthest out from the center, the outermost loop, would be larger in size, leading us to interpret this as indicating that the category it's representing (a particular trick) is somehow larger in general than the other categories (tricks), or larger in magnitude relative to the other categories. So in the case of a day's trick attempts, for instance, the trick with the most attempts might be depicted on the outermost loop while the trick with the least number of attempts is in the innermost loop. So the loops would be ordered from lowest to high values, in this case with respect to the number of total attempts per trick.

What about the size of the bites out of the loops (like bites out of a donut), the share of the loops that's to be vividly colored (successful attempts?) and the share that's less-vividly colored (unsuccessful attempts?)? I have in mind the ratio of successful attempts to total attempts for the vivid region, and for the less-vivid region the ratio of unsuccessful attempts to total attempts.

---

Ah, this type of design seems to be called a [[Radial bar chart|https://datavizcatalogue.com/methods/radial_bar_chart.html]].

[[The R code here seems to produce most of what we want for a radial bar plot|https://jokergoo.github.io/circlize_book/book/high-level-plots.html#circular-barplot]], so long as we don't want to deal with filling in the less-vivid sections (unsuccessful attempts), though I suspect that this linked book includes enough information to figure out how to do that, too. The book also includes [[a short blurb about how you'd produce many of these for day-to-day comparisons|https://jokergoo.github.io/circlize_book/book/advanced-layout.html#arrange-multiple-plots]], as for [[small multiples|https://en.wikipedia.org/wiki/Small_multiple]].