created: 20180407145711804
modified: 20210528013725458
title: Sabbath
tmap.id: 8be3b14e-34e0-4af4-8d55-fdbfcfb94a4c
type: text/vnd.tiddlywiki

Day of resting from Work, or pursuit [of things], which translates to appreciating that which you have, especially the things that are readily available to you, close at hand.

A day filled with dwelling in the knowledge of Goodness, perceiving that God's statement in Genesis that creation was good is yet still relevant, or that creation is good in at least some parts.

---

!! Personal experience
The teaching is that Christians are to go about all their days in a manner of Sabbath-y rest because Jesus' resurrection allowed us all to rest (or something like that). This is all well and good, but I had no idea what this meant, what it involved --- how one does it --- or how it felt (if that's at all important). I set about figuring these things out by attempting to dedicate a whole day to the matter. (Note that I did this out of a sense of willful election, or that I wanted to, rather than that I had to do so (need to). I drew the notion of willfully electing to observe the Sabbath from a TED talk by some guy that decided to practice the Hebrew Bible's teachings and that shared his thoughts about the experience of doing so, mentioning that he enjoyed the Sabbath, especially as he is so prone to workaholism.)

!!! Formative experience for philosophy of work
It was through exploring the Sabbath that I came to develop my first inkling of my philosophy of work.