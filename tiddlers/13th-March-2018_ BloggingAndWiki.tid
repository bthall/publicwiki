created: 20180313213801697
modified: 20210528013723892
tags: Journal
title: 13th-March-2018: BloggingAndWiki
tmap.id: 553425ee-3bb4-477d-b8ee-3d6ee91436a2
type: text/vnd.tiddlywiki

Part of my thoughts regarding [[BloggingAndWiki]], from <https://news.ycombinator.com/item?id=2589897>:

```
_delirium on May 26, 2011 | parent | un-favorite | on: The problem with blogging

I remember being really confused over the chronological ordering for non-diaries when it first become common, and am still not that sure it's the right thing, though it does have benefits for technological convenience (blog software) and lower activation energy (just "write a blog post"). It used to be common to have websites, and then a website might have a "recent updates" or "new articles" page with a reverse-chronologically-ordered list of recent updates, often named new.html or updates.html or something. But it wasn't the main site; just something for frequent visitors to check.

But sometime around 2000-2003 or so, people just started throwing up everything on the equivalent of the "new articles" page! Seemed sort of a strange way to organize anything that wasn't a livejournal. Also seems to indicate a bit of a reduction in long-term ambition: people used to see building a website as an incremental endeavor, where you were slowly building up an edifice, so there was a clear separation between the long-term goal for the site (a resource w/ information presented logically) and the order in which you happened to add each piece (the recent-updates page).

Wikis still have that edifice-building angle (the "Recent Changes" page is clearly not the main page), so maybe it's just that they've taken over that role so completely that the only non-wiki things left are blogs and webapps, with no more "regular" websites?
```

I especially appreciate the line about "long-term ambition" with respect to website creation and, more generally, publishing upon the web:

> people used to see building a website as an incremental endeavor, where you were slowly building up an edifice, so there was a clear separation between the long-term goal for the site (a resource w/ information presented logically) and the order in which you happened to add each piece (the recent-updates page).