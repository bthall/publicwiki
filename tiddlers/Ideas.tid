created: 20180103192740577
icon: $:/core/images/info-button
modified: 20210528013724963
title: Ideas
tmap.id: ad96a527-220e-4171-8e53-f21d078fecc2
type: text/vnd.tiddlywiki

* Offering to provide DataAnalysis services to LibreGratis works/projects.
* I suspect that the below differs a decent bit from the Prospect Theory literature. Prompted by Seth's posts <https://seths.blog/2015/01/uncertainty-risk/> and <https://seths.blog/2010/12/living-with-doubt/>
** [[Uncertainty]]: When it is wildly/radically impractical to predict the outcome of something
*** Often due to a lack of information, knowledge, or understanding or ability to calculate it all when the calculation is needed
** [[Risk]]: When it is practical to predict the outcome of something
* There's such a thing as [[SQL]] files? Release notes for GitAhead 2.5.5 reads "Added syntax highlighting for SQL and Erlang files."
* Found a funny bit about writing a recipe site, in "[[Schlep Blindness|http://www.paulgraham.com/schlep.html]]":

> Probably no one who applied to Y Combinator to work on a recipe site began by asking "should we fix payments, or build a recipe site?" and chose the recipe site.

* Regarding auto-completion in the command line

> The computer doesn't quite know what I'm trying to do, but it's trying to help, and I appreciate that.

* Test my suspicion about the path of one's front knee while ollie-ing
** Can you export the tracked path from physics Tracker software? Would be nice for conducting a meta-analysis across tracked paths.
* Below is a fascinating quote that I didn't expect to find in this article, "[[How to advance when there is no career ladder|http://fortune.com/2014/11/05/career-advancement-advice/]]":

> Young people coming from a college or business school environment may be used to burning the candle at both ends. That doesn’t go over well in a professional setting.
>
> “People notice and it means you’re not at your best,” Tulgan says. “Take care of your mind, take care of your body, take care of your emotional well being…. If you’re really in business for yourself, letting your well-being degrade is like letting the factory fall apart.”

* PublicGoodFinance: Time horizon of funding; Not knowing when X% of your funding will suddenly evaporate (Patreon)
* RSS feed of significant changes (deltas) to wiki contents
* You don't need to do everything yourself [project ideas-wise]. Maybe just let others know of your ideas and let them run with them, possibly making their paths straighter by way of chewing on the problem a bit, yourself, and letting them know what you think up and your thought process for why. This lets you leverage your strengths and the strengths of others, especially given your TimeScarcity and scarcity of other resources. Prioritize.
* Blog systems are publishing systems. I don't know if I've ever really had an issue with publishing things [surely, when the web was new publishing to the web was likely an issue, but since then ...?]; rather, the difficult thing for me has been getting to the point of having something that I need to publish. I might call this a matter of composition, or The Composition Problem. I think that TiddlyWiki is a major step in the right direction towards addressing this problem.
* Opportunity to provide a decently authoritative account of or take upon some particular matter, one that is not bound to simply the first case [instance?] of publication. Lay bare the lingering questions --- those that animate and embarrass the author.
* How is it that experiments are conducted within marketplaces (say, at Facebook) if the structure of the marketplace is often changing due to other experiments being conducted that may or may not be unrelated --- or statistically influential upon the primary experiment?
** Issues of statistically controlling for interference by overlapping experiments
* Tyranny of Now --- Given focus changes; reminded of this notion by reading https://hbr.org/2016/03/you-need-to-practice-being-your-future-self
* IdeasInfrastructure
* release fixed document when the contents are unlikely to warrant significant changes and redirection of consumers of it to revised version (as, with a physical medium, that often isn't possible [efficiently]; and with digital ebook systems without a built-in update system, it might as well be printed on paper) 
* [[Digital divide|https://en.wikipedia.org/wiki/Digital_divide]] and culture, local economies; WorkRemotely and economics
* OpenAccess, LibreSoftware, and [[bias towards what's openly available|https://en.wikipedia.org/wiki/FUTON_bias]]
* For drawing graphs, making art in SVG, check out [[this iOS app|https://itunes.apple.com/us/app/bez-vector-illustration/id1041554256?mt=8]]; should be compatible with iPad Mini
* Pin down which Maths, Programming stuff to learn, & in which sequence --- general DataAnalysis stuff would likely be of use first
* Pin down the concept of Naif/Sophisticate in BehavEcon, as it ties in with my thoughts regarding becoming a better planner, thinker, focuser, etc. (which I think I'm becoming...)
* UI/UX analysis API for app devs, Linux apps especially. Help to identify whether differences in usage behavior (tracked in data, separately) may be significantly linked to UI/UX design or layout on a given platform.
* LaborEconomics and WorkRemotely; LaborEconInProgramming for fun
* In TiddlyWiki, Load images from local path when running local server; else, load canonical URI off remote server. Maybe use JS to change all image loading that involves the remote server (match pattern of remote server's URL/URI) to loading local path. Could add a sidebar toggle for this, as I did with the linking-to of missing tiddlers.
* Integration of sensory data (predicted) as enabling better planning of future actions or investing in matters with future payoffs (BoundedRationality, [[Seasonality]], SensoryData; use of prediction of subjective appraisals of seasonal conditions (weather, comfort, desires --- adventure, growth, physical health preservation during winter, when going outside is tough))
** High costs paid for last-minute purchases; purchase planning as possibly cost-efficient
* Messing around with TextAnalytics, especially DataMining of my own social media posts to extract content from there that'd make for good additions to this wiki
* Niche search engines, or personal ones according to particular networks that you follow
* If there isn't a GUI for it, it doesn't exist.
* Contingent probabilities and business strategy, systems analysis
* PeerTube seedboxes
* Prototyping interaction types via [[Jupyter Widgets|https://www.blog.pythonlibrary.org/2018/10/24/working-with-jupyter-notebook-widgets/]], then move that into Toga GUI

* SocialTourism<<ref "hmmm">>

Need to figure out the "opposite" [[logic|Logic]] terms, like contrapositive/inverse:

* That with race, gender discussions there seems to be a rhetorical form to those discussions that's the opposite of those rhetorical forms that are summed up as //wanting to have one's cake and to eat it, too//, which in that case would mean //to desire to have X, AND to desire to do something that necessitates not-having X// (simultaneously). The opposite form of this, thus, is //to desire to ''not'' have X, AND to desire to do something that necessitates ''having'' X// (simultaneously)

---

* That the brain's pathways are great for relational thinking, but the stronger the seeming relationship between an Antecedent and a Consequent, the more likely that an agent will fall into attempting to ("wrongly," or invalidly) Affirm the Consequent as a (hoped) means of attaining the Antecedent.
** I wonder, //might this general sort of relational self-trust//, or trust in one's established set of relational expectations (possibly akin to the accumulation of scar tissue --- which might be too negatively connotative to proceed with, rhetorically), //explain the HaloEffect// (good and bad)?
* //Boy, you gettin' on my last nerve!// [said recently while making my way through the first of hopefully (genuinely) many more editions of __The Calculus Story: A Mathematical Adventure__ by David Acheson], or proofs that only take you 80% of the way to understanding the whole --- more generally, insufficient argumentation, akin to CurseOfKnowledge, SurelyTheAuthorIsCorrect, HumilityToggling, PresumptionOfConsistency, CoolGuysAreMisunderstood
** This explicitly came to me via reading the post [["I no longer understand my PhD dissertation (and what this means for Mathematics Education)"|https://medium.com/@fjmubeen/ai-no-longer-understand-my-phd-dissertation-and-what-this-means-for-mathematics-education-1d40708f61c]], the below part, in particular:

>I was curious to see how much of the dissertation I can still grasp, five years after the fact. I figured it couldn’t hurt my ego if I refreshed my mind with past mathematical glories.
>
>How wrong I was.
>
>This was not the casual read I had in mind. The notation was alien. I even had to scour the examiner’s report to direct me to the key results. And ''while I could have sworn this was a well-written thesis, I repeatedly found myself bamboozled by my own prompts. “The result now follows easily…” may have made sense back when, but now the author-turned-confused reader can profess that it most certainly does not follow easily, at least in his own mind.'' [//Emphasis mine.//]

* One should not need to be overly beholden to current, local market conditions for their life preservation. One might be wise to seek provisions by way of benefitting many foreign lands (DiversificationBias?), if they wish to also work locally --- lest local conditions deteriorate, leaving them with nothing else to support them.
** EntrepreneursSocial (GrammarAdjectives) can serve to assist with this.

* It is inefficient to do what others could at a lower price, especially when you have before you some many alternative courses of action, some of which would be yet more profitable (to you and/or to society).
** Especially if it is so that one is often conceiving of inventions and/or innovations but possesses not quite the skills to actualize them. To dedicate oneself to acquiring the skills to actualize them may incur some great OpportunityCost, especially if it is relative to the marginal net benefits that'd be derived from coming up with yet another invention or innovation. This is not at all to suggest that one should never seek to actualize and ideas. Rather, one should attempt to be prudent. Perhaps one may spot a pattern across their ideas, such as a feature that exists in all of them, and one may perceive that (with the consideration of ContingentProbabilities) they may learn to develop that feature, which they may then deploy in the actualizing of all their ideas that involve it.  (Not that they necessarily should; see OpportunityCost.)
** Given these concerns, one might provide OpenProjects that people may contribute to --- via formulating one's ideas regarding a project and publicly presenting such for another person to come along and be briefed on what it is and where it might go

* I was for a long time a guy with many ideas but that did little or nothing with them. I've begun to change that, and with all progress that I make in moving away from that condition, I'd like to help others do the same. To do something with an idea is to commit oneself, including one's resources of time and stress, but it need not be so painful or difficult, I think.

<<footnotes "hmmm" "Yeah, we should likely look into this more">>