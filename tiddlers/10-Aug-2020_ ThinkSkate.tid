created: 20200810194752032
modified: 20210528013723715
tags: ThinkSkate Journal
title: 10-Aug-2020: ThinkSkate
tmap.id: 2ddff7a3-4c39-4f8a-b238-f94baed9abc9
type: text/vnd.tiddlywiki

I've made some progress with learning how to output the raw analytical tables so I can share them here! (Source code is at the very bottom of the post; I'll track it via Git soon.)

From the data contained in the file [[Skate Metrics - Base Data.csv]], I have summarized the trick data down to the table below. Note that this table sorts by which tricks I've landed the most to the least given all my attempts ("Hits to Attempts Ratio", or Times Landed divided by Num of Attempts).

At the time of posting, the data pertains to 4 days of data collection (I'm lagging on inputting all the hand-recorded data), which accounted for 88 trick attempts. (I'm not certain yet whether I will update the files (both of them, only one, or neither), so what you see below might look different than it does when I'm writing this post if both files diverge.)

{{Trick Hits to Attempts.csv}}


!! Source code (R)
```
setwd("~/Documents/")

# Handle NA strings: https://stackoverflow.com/questions/24172111/change-the-blank-cells-to-na#24172277
data <- read.csv(file = 'Skate Metrics.csv', na.strings=c(""," ","NA"))
head(data)

library(tidyr)
library(data.table)
str(data)

# as.Date : https://stackoverflow.com/questions/47230146/error-message-do-not-know-how-to-convert-dataframecol-to-class-date?rq=1
data$Date <- as.Date(data$Date, format="%m/%d/%Y")

data <- fill(data, c("Date", "Trick"), .direction = "down")

write.csv(data,"Skate Metrics - Base Data.csv",row.names = FALSE, quote = FALSE) # write base data to csv file

df = data.table(data)

# Order: https://stackoverflow.com/questions/12353820/sort-rows-in-data-table-in-decreasing-order-on-string-key-order-x-v-gives-er
data2 <- df[,.(sum(Outcome)/length(Outcome),sum(Outcome),length(Outcome)),by=c("Trick")][order(-V1)]
colnames(data2) # [1] "Trick" "V1"    "V2"    "V3"   

setnames(data2, colnames(data2), c("Trick","Hits to Attempts Ratio","Times Landed","Num of Attempts"))

# data2[,c(1:3,5)]

data2

write.csv(data2,"Trick Hits to Attempts.csv",row.names = FALSE, quote = FALSE) # write summary to csv file

# Number of days of tricks recorded
length(unique(data$Date))

# Number of attempts in dataset
sum(data2[,4])

```