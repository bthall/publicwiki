created: 20200827013636411
modified: 20210528013724326
tags: Economics Journal EconomicTheory Notes Podcasts
title: 26-Aug-2020-economics
tmap.id: ea539077-e5d2-4655-a62e-d6cd70dd0e39
type: text/vnd.tiddlywiki

I was quite glad to listen to the econ theory discussion in [[this podcast episode|https://hetpodcast.libsyn.com/episode-thirty-five]], especially towards the end when they were discussing the importance and use of ceteris paribus assumptions within economics.

The scholar on the panel that spoke about this said that the ceteris paribus assumptions tend to be used so that a benchmark may be established, one against which comparisons may be made (as with comparative statics), even though economists are nonetheless reluctant to suggest that markets are in equilibrium or that the market conditions do in fact match those suggested by the ceteris paribus conditions. (This here prompted me to think at first, wrongly, that a market that is either in equilibrium is necessarily in conditions matching the ceteris paribus conditions and, the reverse, that a market that is in conditions matching the ceteris paribus conditions is necessarily in equilibrium. This need not be so, and this actually allows for all the undergraduate studies that deal with analyzing departures from equilibrium states.) This idea agrees with my understanding of how they are used in practice, at least given my experience from my undergraduate studies.

In relation to this, they brought up that Hayek suggested that an equilibrium state is unattainable because market data (supply/demand) would always be changing, making difficult to the point of being impossible to recognize whether an equilibrium exists or could exist given some policies --- policies which, I'm guessing, would need to assume a degree of unchangingness in the general pattern of the data (think: modeling) in order to feel correct in pursuing them.